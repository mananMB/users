// ==== Problem #1 ====
// The owner of the company wants to find the details of the user with id 100.
// Help the dealer find out which user has an id of 100 by calling a function that will return the data for that user.
// Then log the car's name, email, and gender in the console log in the format of:
// "*Username goes here* is a *user gender goes here* and can be contacted on *user email goes here*"

const problem1 = (id, users) => {
  for (let index = 0; index < users.length; index++) {
    if (users[index].id === id) {
      return `${users[index].first_name} ${users[index].last_name} is a ${users[index].gender} and can be contacted on ${users[index].email}`;
    }
  }
};

module.exports = problem1;
