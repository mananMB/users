// ==== Problem #4 ====
// The marketing team needs all the emails from every user on the list.
// Execute a function that will return an array from the user data containing
// only the emails and log the result in the console as it was returned.

const problem4 = (users) => {
  let emailList = [];
  for (const usersKey in users) {
    emailList.push(users[usersKey].email);
  }
  return emailList;
};

module.exports = problem4;
