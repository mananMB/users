const expect = require("chai").expect;
const problem3 = require("../problem3");
const users = require("../users");

let usersCopy = [];

for (let index = 0; index < users.length; index++) {
  usersCopy.push(users[index]);
}

usersCopy = usersCopy.sort((a, b) => {
  let fa = a.last_name.toLowerCase();
  let fb = b.last_name.toLowerCase();
  if (fa < fb) {
    return -1;
  }
  if (fa > fb) {
    return 1;
  }
  return 0;
});

describe("Test Problem 3", function () {
  it("should return User object literal sorted by last name", function () {
    let result = problem3(users);
    expect(result).to.deep.equal(usersCopy);
  });
});
