const expect = require("chai").expect;
const problem4 = require("../problem4");
const users = require("../users");

let expectedEmailIDArray = [];
for (const usersKey in users) {
  expectedEmailIDArray.push(users[usersKey].email);
}
describe("Test Problem 4", function () {
  it("should return array containing all the user email ids", function () {
    const result = problem4(users);
    expect(result).to.deep.equal(expectedEmailIDArray);
  });
});
