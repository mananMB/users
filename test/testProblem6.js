const expect = require("chai").expect;
const problem6 = require("../problem6");
const users = require("../users");

let genderList = [
  "Male",
  "Female",
  "Polygender",
  "Bigender",
  "Genderqueer",
  "Genderfluid",
  "Agender",
];

let usersCopy = [];

for (const user in users) {
  usersCopy.push(users[user]);
}

let expectedResults = [];

for (const gender in genderList) {
  let thisGenderList = [];
  for (const user in usersCopy) {
    if (usersCopy[user].gender === genderList[gender]) {
      thisGenderList.push(usersCopy[user]);
      usersCopy.splice(Number(user), 1);
    }
  }
  expectedResults.push(thisGenderList);
}

describe("Test Problem 6", function () {
  it("should return an array containing array of objects in the required order", function () {
    const result = problem6(users);
    expect(result).to.deep.equal(expectedResults);
  });
});
