const expect = require("chai").expect;
const problem5 = require("../problem5");
const users = require("../users");

let expectedResult = [];

for (const usersKey in users) {
  if (users[usersKey].gender.toLowerCase() === "male") {
    expectedResult.push([
      users[usersKey].first_name,
      users[usersKey].last_name,
      users[usersKey].email,
    ]);
  }
}

describe("Test Problem 4", function () {
  it("should return array containing name and email ids of all male users", function () {
    const result = problem5(users);
    expect(result).to.deep.equal(expectedResult);
  });
});
