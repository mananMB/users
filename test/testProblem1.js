const expect = require("chai").expect;
const problem1 = require("../problem1");
const users = require("../users");

describe("Test Problem 1", function () {
  it("should return details of user id provided", function () {
    const result = problem1(100, users);
    expect(result).to.be.equal(
      "Corrina Nussey is a Genderfluid and can be contacted on cnussey2r@examiner.com"
    );
  });
});
