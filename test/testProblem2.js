const expect = require("chai").expect;
const problem2 = require("../problem2");
const users = require("../users");

describe("Test Problem 2", function () {
  it("should return details of last user", function () {
    const result = problem2(users);
    expect(result).to.be.equal(
      "Last user is Gwynne and can be contacted on gstoffelsrr@mashable.com"
    );
  });
});
