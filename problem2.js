// ==== Problem #2 ====
// The owner of the company wants the information on the last user in the list.
// Execute a function to find what the name and email of the last user in the inventory is.
// Log the name and email into the console in the format of:
// "Last user is *username goes here* and can be contacted on *user email goes here*"

const problem2 = (users) => {
  let lastIDIndex = -1;
  for (let index = 0; index < users.length; index++) {
    if (lastIDIndex < users[index].id) lastIDIndex = index;
  }
  return `Last user is ${users[lastIDIndex].first_name} and can be contacted on ${users[lastIDIndex].email}`;
};

module.exports = problem2;
