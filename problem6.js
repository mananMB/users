// ==== Problem #6 ====
// The metrics team is interested in seeing how many users fall into which gender.
// Execute a function and return an array of arrays where each array contains the user information
// of a particular gender in the order Male, Female, Polygender, Bigender, Genderqueer, Genderfluid, Agender.
// Once you have the array, use JSON.stringify() to show the results of the array in the console.

const problem6 = (users) => {
  let genderList = [
    "Male",
    "Female",
    "Polygender",
    "Bigender",
    "Genderqueer",
    "Genderfluid",
    "Agender",
  ];

  let usersCopy = [];

  for (const user in users) {
    usersCopy.push(users[user]);
  }

  let arraySet = [];

  for (const gender in genderList) {
    let thisGenderList = [];
    for (const user in usersCopy) {
      if (usersCopy[user].gender === genderList[gender]) {
        thisGenderList.push(usersCopy[user]);
        usersCopy.splice(Number(user), 1);
      }
    }
    arraySet.push(thisGenderList);
  }
  return arraySet;
};

module.exports = problem6;
