// ==== Problem #3 ====
// The marketing team wants the users listed alphabetically on the website by last name.
// Execute a function to Sort all the user's names into alphabetical order
// and log the results in the console as it was returned.

const problem3 = (users) => {
  let usersCopy = [];

  for (let index = 0; index < users.length; index++) {
    usersCopy.push(users[index]);
  }

  return usersCopy.sort((a, b) => {
    let fa = a.last_name.toLowerCase();
    let fb = b.last_name.toLowerCase();
    if (fa < fb) {
      return -1;
    }
    if (fa > fb) {
      return 1;
    }
    return 0;
  });
};

module.exports = problem3;
