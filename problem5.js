// ==== Problem #5 ====
// The marketing manager needs to find out how many users are male.
// Find out how many users are male and return the array of names and emails and log their length.

const problem5 = (users) => {
  const maleUserDetails = [];
  for (const user in users) {
    if (users[user].gender.toLowerCase() === "male") {
      maleUserDetails.push([
        users[user].first_name,
        users[user].last_name,
        users[user].email,
      ]);
    }
  }
  return maleUserDetails;
};
module.exports = problem5;
